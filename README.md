## A propos

Projet réalisé par Greg Beaujois et Adrien Paviot.

## Lancement du programme

Stack est nécessaire pour pouvoir executer le programme.

	stack run [fichier de graph]
	exemple: stack run data/test.vc


## Algorithme
    
2 versions sont implémentées dans le programme.
    
- **Version 1**

Cette version ne répond pas complétement au problème posé. Elle retourne un ensemble de sommet qui permet de couvrir les arretes du graph
mais la solution retournée n'est pas obligatoirement cette contenant le moins de sommets.
    
###### L'algorithme est le suivant:
    
1.  Nous récupérons le sommet qui a le plus de voisins
2.  Nous stockons ce sommet dans un tableau
3.  On supprime le sommet du graph ainsi que toutes les arretes qui le concernaient
4.  On recommence jusqu'à ce qu'il n'y ai plus d'arretes
5.  On retourne le tableau qui contient tous les sommets sélectionnés
    
    
    
-  **Version 2**

N'ayant pas bien compris l'algorithme qui nous avait été présenté lors de la dernière séance de TP,
Romain PHET nous a aidé à comprendre l'algorithme. C'est la raison pour laquelle une partie des fonctions de cette
deuxième version sont ressemblantes avec les siennes. Néanmoins, nous les avons implémentées à notre
façon. Son aide nous a uniquement permis de comprendre l'algorithme.
    
###### L'algorithme est le suivant:
    
###### Calcul du coût

Pour un ensemble de sommet le coût est calculé en additionant la taille des sommets précédémment sélectionnés, la taille des sommets candidats et
le nombre de chemins partant de ces sommets (précédémment séléctionnés et candidats).
    
###### Initialisation
    
1.  Nous récupérons le sommet avec le plus de voisin
2.  Nous calculons le coût pour ce sommet et pour les voisins de ce sommet
3.  Nous insérons les deux cas dans un tas dont la priorité est le coût calculé précédemment.
    On insert donc un tableau contenant le sommet avec le plus de voisins et un tableau avec les voisins de ce sommet.
    
    
###### Déroulement de l'algorithme
    
4.  On récupère la tête du tas (avec le coût le plus faible)
5.  On calcul un nouveau graph en enlevant les sommets sélectionnés depuis la tête du tas
6.  On calcul le sommet avec le plus de voisins dans le graph calculé précédemment.
7.  On calcul le coût pour:
    *   les sommets sélectionnés depuis la tête du tas concaténé au sommet récupéré
    *   les sommets sélectionnés depuis la tête du tas concaténé aux sommets voisins du sommet récupéré
8.  On insert les deux solutions dans le tas
    
On répète l'opération jusqu'à ce que le graph de l'étape 4 ne possède plus d'arretes