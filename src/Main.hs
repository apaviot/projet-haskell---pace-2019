module Main where

import qualified Fr.Umlv.Pace2019.Parser as Parser
import qualified Fr.Umlv.Pace2019.Graph  as Graph
import qualified Fr.Umlv.Pace2019.Pace   as Pace
import qualified System.Environment      as Environment
import qualified System.IO               as IO
import qualified Data.List               as L
import qualified Data.Maybe              as M



main :: IO ()
main = do
    args    <- Environment.getArgs
    content <- IO.readFile (L.head args)
    IO.putStr "\nParsing du graph\n"
    let graph = M.fromJust . Parser.parse $ L.lines content
    let a = Pace.vertexCover graph
    IO.putStr "Calcul des sommets couvrants\n"
    IO.putStr "Résultat: "
    IO.putStr . show $ Pace.vertexCover graph
    {-
    IO.putStr "\nRésultat: "
    IO.putStr . show $ Pace.vertexCover' graph
    -}
    IO.putStr "\n"
    
