module Fr.Umlv.Pace2019.Pace
(
    vertexCover, vertexCover'
) where

import qualified Fr.Umlv.Pace2019.Graph as G
import qualified Data.Heap              as H
import qualified Data.List              as L
import qualified Data.Maybe             as M
import qualified Data.Set               as S
import qualified Debug.Trace            as Debug

{-
Version 1: L'algorithme sort un résultat mais le résultat n'est pas forcément celui contenant le moins de sommet
-}
vertexCover' :: G.Graph -> [G.Vertex]
vertexCover' g = L.sort $ vertexCoverRecursive' g []


-- Renvoi un tableau de Vertex permettant de couvrir l'ensemble des arretes du graph
vertexCoverRecursive' :: G.Graph -> [G.Vertex] -> [G.Vertex]
vertexCoverRecursive' g vs
    | not (G.hasEdges g) = vs
    | otherwise = vertexCoverRecursive' (G.removeVertex elt g) (elt:vs)
        where
            elt = G.hasMostNeighbours g


{-
Version 2: L'algorithme expliqué au tableau
-}

-- Renvoi un tableau de Vertex permettant de couvrir l'ensemble des arretes du graph
vertexCover :: G.Graph -> [G.Vertex]
vertexCover g 
    | (not (G.hasEdges g)) = []
    | otherwise = L.sort $ vertexCoverRecursive g (initialisation g) []


-- Effectue la premiere étape de l'algorithme
initialisation :: G.Graph -> H.MinPrioHeap Int [G.Vertex]
initialisation g = H.insert (costLeft, [mostCommon]) $ H.insert (costRight, neighbours) heap
    where
        heap = H.empty :: H.MinPrioHeap Int [G.Vertex]
        mostCommon = G.hasMostNeighbours g
        neighbours = G.getNeighboursFromVertices [mostCommon] g
        costLeft = (L.length neighbours) + 1
        costRight = (L.length neighbours) + (G.nbEdgesFromVertices neighbours g)


vertexCoverRecursive :: G.Graph -> H.MinPrioHeap Int [G.Vertex] -> [[G.Vertex]] -> [G.Vertex]
vertexCoverRecursive g h try
    | (not (G.hasEdges currentGraph)) = selectedVertices
    | otherwise = vertexCoverRecursive g (fst insertion) (snd insertion)
    where
        heapHead = head $ H.take 1 h
        selectedVertices = snd heapHead
        mostCommon = G.hasMostNeighbours currentGraph
        currentGraph = G.removeVertices selectedVertices g
        selectedVerticesLeft = mostCommon:selectedVertices
        selectedVerticesRight = S.toList . S.fromList $ foldr (\a b -> a:b) selectedVertices (G.getNeighboursFromVertices selectedVerticesLeft g)
        costLeft = cost selectedVerticesLeft g
        costRight = cost selectedVerticesRight g
        insertion = insertionHeap costLeft selectedVerticesLeft $ insertionHeap costRight selectedVerticesRight ((H.drop 1 h), try)


-- Calcul le coût pour un ensemble de sommets
cost :: [G.Vertex] -> G.Graph -> Int
cost xs g = (L.length xs) + (G.nbEdgesFromVertices xs g)

insertionHeap :: Int -> [G.Vertex] -> (H.MinPrioHeap Int [Int], [[G.Vertex]]) -> (H.MinPrioHeap Int [G.Vertex], [[G.Vertex]])
insertionHeap cost selectedVertices x@(heap, try)
    | selectedVertices `L.elem` try = x
    | otherwise = (H.insert (cost, selectedVertices) heap, selectedVertices:try)

