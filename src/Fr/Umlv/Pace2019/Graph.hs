module Fr.Umlv.Pace2019.Graph
(
    Graph, Vertex, Edge,
    empty,
    addEdge,
    hasMostNeighbours,
    removeVertex,
    removeVertices,
    getNeighbours,
    getNeighboursFromVertices,
    degreeFromVertices,
    nbEdgesFromVertices,
    hasEdges
) where
  
import qualified Data.Foldable                 as F
import qualified Data.List                     as L
import qualified Data.Maybe                    as Maybe
import qualified Data.IntMap                   as Map
import qualified Data.Set                      as S

type Edge = (Vertex, Vertex)
type Vertex = Int
type Graph = Map.IntMap [Vertex]

-- Construit un Graph vide
empty :: Graph
empty = Map.empty

-- Ajoute un arc au graph
addEdge :: Edge -> Graph -> Graph
addEdge (v1, v2) g = insert v1 v2 $ insert v2 v1 g
    where
        insert key val map = Map.insert key (newValue key val map) map
        newValue key val map = val:(Map.findWithDefault [] key map)


-- Supprime un sommet du graph et retourne le graph modifié
removeVertex :: Vertex -> Graph -> Graph
removeVertex v g = removeEdgesFromVertex v $ Map.delete v g


-- Supprime les arcs qui partent d'un sommet et renvoi le graph modifié
removeEdgesFromVertex :: Vertex -> Graph -> Graph
removeEdgesFromVertex v g = Map.mapMaybeWithKey filter g
    where
        filter k vs
            | k == v = Just []
            | otherwise = Just [x | x <- vs, x /= v]


-- Supprime plusieurs sommets d'un graph et retourne le nouveau graph
removeVertices :: [Vertex] -> Graph -> Graph
removeVertices (x:xs) g
    | xs == [] = removeVertex x g
    | otherwise = removeVertices xs $ removeVertex x g


-- Retourne le sommet qui a le plus de voisins
hasMostNeighbours :: Graph -> Vertex
hasMostNeighbours g = Maybe.fromJust . Map.foldlWithKey f Maybe.Nothing $ m
    where
        m = Map.map (\v -> L.length v) g
        f acc k v
            | acc == Maybe.Nothing = Maybe.Just k
            | otherwise = if v > (L.length $ g Map.! justAcc) then (Maybe.Just k) else acc
                where
                    justAcc = Maybe.fromJust acc


-- Retourne la liste des voisins d'un sommet
getNeighbours :: Vertex -> Graph -> [Vertex]
getNeighbours v g = Maybe.fromJust $ Map.lookup v g


-- Retourne l'ensemble des voisions de plusieurs sommets
getNeighboursFromVertices :: [Vertex] -> Graph -> [Vertex]
getNeighboursFromVertices xs g = filter (\x -> not (x `L.elem` xs)) . S.toList . S.fromList $ L.concatMap (\x -> getNeighbours x g) xs


{- 
Retourne nombre total des arcs qui sortent de plusieurs sommets
Un arc (a, b) est comptabilisé dans le calcul du degré de a ET de b
-}
degreeFromVertices :: [Vertex] -> Graph -> Int
degreeFromVertices vs g = Map.foldr (\v acc -> acc + (L.length v)) 0 (Map.filterWithKey (\k v -> k `elem` vs) g)


-- Retourne le nombre d'arretes qui partent d'un ensemble de sommet
nbEdgesFromVertices :: [Vertex] -> Graph -> Int
nbEdgesFromVertices (x:[]) g = L.length . Maybe.fromJust $ Map.lookup x g
nbEdgesFromVertices (x:xs) g = (L.length .Maybe.fromJust $ Map.lookup x g) + (nbEdgesFromVertices xs (removeVertex x g))


-- Retourne True si le Graph possède au moins un arc, False sinon
hasEdges :: Graph -> Bool
hasEdges g = 0 < (Map.size $ Map.filter (\k -> L.length k > 0) g)