module Fr.Umlv.Pace2019.Parser
(
    parse
) where

import qualified Fr.Umlv.Pace2019.Graph as Graph
import qualified Data.Foldable          as F
import qualified Data.List              as L
import qualified Data.Tuple             as T
import qualified System.Environment     as Environment
import qualified System.IO              as IO
import qualified Data.Maybe             as Maybe

-- |'stringToInt' 's' converts the string 's' to integer
stringToInt :: String -> Int
stringToInt s = read s :: Int

-- Convenience function.
whenMaybe :: a -> Bool -> Maybe a
whenMaybe _ False = Nothing
whenMaybe a True  = Just a

-- |'parseG' 's' parses a graph given as a PACE 19 format list of strings.
-- The function returns 'Nothing' in case of parse error.
parse :: [String] -> Maybe Graph.Graph
parse = go Graph.empty . L.map L.words . L.filter (not . L.isPrefixOf "c")
    where
        go g []                    = Maybe.Just g
        go g (["p","td",n,m] : ls) = go g ls
        go g ([i,j]          : ls) = go (Graph.addEdge (stringToInt i, stringToInt j) g) ls
        go _ _                     = Nothing